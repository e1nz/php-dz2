<?php

//    1. При помощи оператора if посчитать стоимость доставки почтой.
//       Создадим две переменные $company = 'NovaPoshta'; $total = 0;
//       - Если $company равно NovaPohta то к переменно $total добавляем 100
//       - Или если $company равно UkrPoshta то к перменно $total добавляем 145
//       - Иначе в любом другом случае к переменно $total добавляем 200
//
//       После оператор if вывести итоговое сообщение с таким текстом (перменные подставить на свои места):
//       echo '<h1>Delivery company: NovaPoshta, Total price: 10 UAH</h1>';

    $company = 'NovaPoshta';
    $total = 0;
    if  ( $company == 'NovaPoshta') {
        $total = 100;
    } elseif ( $company == 'UkrPoshta') {
        $total = 145;
    } else {
    $total = 200;
    }
    echo '<h1>Delivery company: '.$company.', Total price: '.$total.' UAH</h1><br>';
    echo '<hr>';

//    2. При помощи оператора if посчитать надбавку от выработки стажа и пола.
//       Создадим три переменные: $gender = 'men'; $year = 10; $sum = 0;
//       - Если $gender равен men и $year больше 10 и меньше 20, то прибавляем к переменной $sum 1000
//       - Или если $gender равен women и $year больше 10 и меньше 15, то прибавляем к переменной $sum 1500
//       - Или если $year больше 20 и меньше 30, то прибавляем к переменной $sum 2500
//       - Иначе прибавляем к перемнной $sum 5000;

    $gender = 'men';
    $year = 10;
    $sum = 0;

    if ($gender == 'men' && $year > 10 && $year < 20) {
        $sum = 1000;
    } elseif ($gender == 'women' && $year > 10 && $year < 15) {
        $sum = 1500;
    } elseif ($year > 20 && $year <30) {
        $sum = 2500;
    } else {
        $sum = 5000;
    }

    echo $sum;
    echo '<hr>';

//    3. При помощи оператора switch вывести сообщение преветствия для пользователя.
//       Создадим переменную $username = 'Alex';
//       - Для пользователя Alex выводим на экран 'Hello Alex';
//       - Для пользователей Anna, Jon, Dima выводим на экран 'Hello my friends!';
//       - В любом другом случае выводим на экран 'Hello new user!';

    $username = 'Alex';
    switch ($username) {
        case 'Alex':
            echo "Hello Alex!";
            break;
        case 'Anna':
        case 'Jon':
        case 'Dima':
            echo "Hello my friends!";
            break;
        default:
            echo "Hello new user!";
    }
    echo '<hr>';

//    4. Выведите столбец чисел от 0 до 14 с переходом на новую строку при помощи цикла for.

    for ($i=0; $i<15; $i++) {
        echo $i . '<br>';
    }
    echo '<hr>';

//    5. Создайте массив $arr1 = []; Наполните его целыми числами от 10 до 20 при помощи цикла for.

    echo '<pre>';
    $arr1 = [];
    for ($i=10; $i<21; $i++) {
        $arr1[] = $i;
    }
    var_dump($arr1);
    echo '</pre><hr>';

//    6. Дан массив $arr2 = [50, 60, 70, 80, 13, 21, 49]; Найдите сумму всех элементов при помощи цикла for и выведите
//       ее на экран. Можно использовать доп переменные.

    $arr2 = [50, 60, 70, 80, 13, 21, 49];
    $count2 = count($arr2);
    $a=0;
    for ($i=0; $i < $count2; $i++) {
        $a = $a + $arr2[$i];
    }
    echo $a . '<hr>';

//    7. Дан массив $arr3 = [-10, 15, 4, 2, -5, -3, 8]; Сложите сумму отдельно всех положительных и отрицательных значений.
//       При помощи цикла for и операторва ветвления if. Выведите на экран сумму положительных и отрицательных отдельно.

    $arr3 = [-10, 15, 4, 2, -5, -3, 8];
    $count3 = count($arr3);
    $b=0;
    for ($i=0; $i < $count3; $i++) {
        if ($arr3[$i] > 0) {
            $b = $b + $arr3[$i];
        }
            else {
                $c = $c + $arr3[$i];
            }
    }
    echo $b . '<br>';
    echo $c . '<hr>';

//    8. Есть массив $users. Выведите на экран соообщение для каждого элемента массива при помощи цикла for:
//       echo '<h1>User: alex, Email: test@gmail.com</h1></br>';

       $users = [
            [
                'username'  => 'alex',
                'email' => 'test@gmail.com'
            ],
            [
                'username'  => 'nina',
                'email' => 'ninaexample@gmail.com'
            ],
            [
                'username'  => 'olga',
                'email' => 'olga@gmail.com'
            ]
       ];

       $count4 = count($users);
       for ($i=0; $i < $count4; $i++) {
           echo '<h1>User: '. $users[$i]['username'] . ', Email: ' . $users[$i]['email'] . '</h1></br>';
       }