<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* 1. Вызовите ф-цию aloha и передайте ей значение. */
        function aloha($name) {
            echo 'Aloha my dear, ' . $name . '!';
        }
        aloha('Alex');
        echo '<hr>';

/*
    2. Доработать ф-цию ola и задайте значение по умолчанию, если в аргумент $name нечего не приходит, чтоб подставлялось
    слово friend. Вызовите ф-цию ola.
*/
        function ola($name = 'friend') {
            echo 'Ola my dear, ' . $name . '!';
        }
        ola();
        echo '<hr>';

/* 3. Доработайте ф-цию salam так чтоб, в аргумент $name можно было передать только строку. Вызовите ф-цию salam. */
        function salam(string $name) {
            echo 'Salam my dear, ' . $name . '!';
        }
        salam('Maksym');
        echo '<hr>';

/* 4. Доработать ф-цию: чтоб любое изменение перменной $var в локальном контексте, отразилось на ее состояние в глобальном контексте */
        $var = 173;
        $number = 0;
        function plusTen(&$number) {
            $number += 10;
        }
        plusTen($var);
        echo $var;
        echo '<hr>';

/*
    5. Разработать ф-цию для расчета площади квадрата по стороне:
        5.1 Ознакомиться с формулой по ссылке: https://infofaq.ru/ploshhad-kvadrata.html
        5.2 Сделать так чтоб аргумент $a принимал числовые значения
        5.3 Расчитать перменную $result и вернуть ее значение из фу-ции
        5.4 Вызвать функцию squareArea
*/
        $result = 0;
        function squareArea(int $a) {
            $result = $a * $a;
            return $result;
        }
        echo squareArea(5);
        echo '<hr>';

/*
    6. Доработать фу-цию расчета идеального веса по формуле 'Брокка':
        6.1 Ознакомиться с формулой по ссылке: https://aif.ru/health/food/kak_nayti_svoy_idealnyy_ves_5_sposobov_i_formuly_raschyota
        6.2 Расчитать значение перменной $weight для каждого из ветвлений ифа в зависимости от $gender
        6.3 Фу-ция по ее завершению должна возвращать значение из перменной $weight
        6.4 Для аргумента $index задать значение по умолчанию 1.15
        6.5 Аргумент $gender должен принимать строкоые значения, $height целочисленные, $index значения с плавующей точкой
        6.6 Вызвать ф-цию getWeight с набором значений
*/
        $weight = 0;
        function getWeight(string $gender, int $height, float $index = 1.15) {
            if ($gender == 'men') {
                $weight = ($height - 100) * $index;
            } else {
                $weight = ($height - 110) * $index;
            }
            return $weight;
        }
        echo getWeight('men', 176);
        echo '<hr>';

/* 7. Разработать функцию которая будет считать среднее значение среди суммы всех элементов массива. */

        $arr = [2, 4, 6, 8];
        $avg = 0;
        function average(array $data) {
            $avg = array_sum($data) / count($data);
            return $avg;
        }
        echo average($arr);

//$arr = [10, 5, 15];
//$avg = average($arr); // Должно получится 10
//echo $avg;
